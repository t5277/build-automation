#!/bin/bash
#
#	Build the RPi4 firmware from source
#

#-- basics: some global values
BLD_LOG="bld.log"		# will end up in $WORKSPACE
BUILD_ROOT="build"
GITLAB_ROOT="https://gitlab.com/t5277"
PACKAGES="git git-subtree gcc-aarch64-linux-gnu libuuid-devel"
SUBTREES="\
	edk2 main \
	edk2-platforms main \
	edk2-non-osi main \
	uefi-tools f35 \
"
WORKSPACE=""
UPDATING=""

#-- helper functions
function check_packages () {
	# look for some expected packages
	missing=""
	while [ $1 ]
	do
		ii="$1"
		shift
		info=$(rpm -qa | grep "$ii")
		if [ -z "$info" ]
		then
			missing=" $ii"
		fi
	done

	if [ -z "$missing" ]
	then
		echo "! all packages found"
	else
		echo "? missing packages, please install:"
		echo "  $ sudo dnf install${missing}"
		exit 100
	fi
}

function fetch_sources () {
	repo="${GITLAB_ROOT}/rpi4-subtrees.git"
	branch="fedora"
	if [ -z "$UPDATING" ]
	then
		git clone --quiet -b $branch $repo $WORKSPACE
		[ $? != 0 ] && exit 100
		echo "! cloned $repo"
	fi
	cd $WORKSPACE
	git checkout $branch
	[ $? != 0 ] && exit 110
	git pull --quiet
	[ $? != 0 ] && exit 120
	echo "! updated $repo"
}

function fetch_submodules () {
	# first, update the source trees
	while [ $1 ]
	do
		modpath="$1"
		shift
		modbranch="$1"
		shift

		pushd $WORKSPACE/$modpath 2>&1 >/dev/null
		if [ -z "$UPDATING" ]
		then
			git submodule update --init --quiet
			[ $? != 0 ] && exit 200
		fi
		git submodule sync --quiet
		[ $? != 0 ] && exit 210
		git submodule update --quiet
		[ $? != 0 ] && exit 220
		git checkout $modbranch --quiet
		[ $? != 0 ] && exit 230
		git pull --quiet
		[ $? != 0 ] && exit 240
		echo "! updated submodule: $modpath"
		popd 2>&1 >/dev/null
	done
}

function build_uefi () {
	# sources are all present, as are the build dependencies.
	# so, build EDK2
	LOGFILE="$1"
	pushd $WORKSPACE 2>&1 >/dev/null
	echo "===== EDK2 Build Start: $(date -u) ======================" | \
		tee $LOGFILE
	./uefi-tools/edk2-build.sh rpi4 2>&1 | tee -a $LOGFILE
	echo "===== EDK2 Build End: $(date -u) ======================" | \
		tee -a $LOGFILE
	popd 2>&1 >/dev/null
}

function usage () {
	echo "usage: $(basename $0) [<path>]"
	echo "where:"
	echo "    <path> => where to build the edk2 firmware"
	echo "              (assumes 'build' if not given)"
}

#-- main: do all the things
if [ $# -gt 0 ]
then
	WORKSPACE="$(realpath $1)"
else
	WORKSPACE="$(realpath ${BUILD_ROOT})"
fi
export WORKSPACE

if [ -d "$WORKSPACE" ]
then
	UPDATING="TRUE"
	echo "! updating workspace: $WORKSPACE"
else
	echo "! building workspace: $WORKSPACE"

	#-- make sure we can build things properly
	check_packages $PACKAGES
fi

#-- build up the source trees
fetch_sources

#-- build the firmware
build_uefi ${WORKSPACE}/${BLD_LOG}

